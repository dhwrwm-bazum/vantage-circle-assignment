/**
 * @format
 */

import {AppRegistry} from 'react-native';
import List from './components/List';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => List);
