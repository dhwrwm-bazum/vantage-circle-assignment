import React, {Component} from 'react';
import { Text, View, ScrollView, ActivityIndicator, Platform } from 'react-native';
import ListStyle from './ListStyle';
import CardContainer from './Card';

export default class App extends Component{
  constructor(props){
    super(props);

    this.state = {
      interviewData: [],
      error: {},
      isLoading: false
    }

    this.getInterviewData = this.getInterviewData.bind(this);
    this.onScroll = this.onScroll.bind(this);
  }

  componentDidMount(){
    this.getInterviewData();
  }

  getInterviewData(){
    this.setState({
      isLoading: true
    });

    fetch("https://us-central1-webpush-151905.cloudfunctions.net/getInterviewData")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoading: false,
            interviewData: [...this.state.interviewData, ...result.data]
          });
        },
        (error) => {
          this.setState({
            isLoading: false,
            error
          });
        }
      )
  }

  onScroll({nativeEvent}){

    if(this.isCloseToBottom(nativeEvent) && !this.state.isLoading){
      this.getInterviewData();
    }
  }

  isCloseToBottom({layoutMeasurement, contentOffset, contentSize}) {  
    return Platform.OS === 'ios' ? layoutMeasurement.height + contentOffset.y >= contentSize.height + 5:
      layoutMeasurement.height + contentOffset.y >= contentSize.height - 5; 
  }

  render() {
    const { isLoading, interviewData, error } = this.state;

    if(isLoading && Object.keys(interviewData).length === 0){
      return (
        <View style={[ListStyle.container, { alignItems: 'center', justifyContent: 'center'}]}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    }

    return (
      <View style={ListStyle.container}>
        {
          Object.keys(error).length === 0  &&
            <ScrollView onScroll={this.onScroll} scrollEventThrottle={16}>
              {
                interviewData.map((item, index) => <CardContainer key={index} data={item}/>)
              }
            </ScrollView>

        }

        {
          Object.keys(error).length > 0 &&
            <Text>Api Error</Text>
        }

        {
          isLoading &&
            <View style={{padding: 10}}>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
        }
        
      </View>
    );
  }
}
