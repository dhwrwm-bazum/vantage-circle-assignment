import React from 'react';
import { Text, View, Image } from 'react-native';
import CardStyle from './CardStyle';
import { Card } from 'native-base';
var moment = require('moment');

export default CardContainer = ({data}) => {
  const { itemHeadline, itemText, img, isTrending, expiryDate } = data;
  const urlSplitted = img.split('/');
  const imgName = urlSplitted[urlSplitted.length - 1].split('.')[0]; 
  return (
    <Card>
      <View style={CardStyle.container}>
        <View style={CardStyle.imgContainer}>
          <Image source={{uri: img}} style={CardStyle.itemLogo}/>
        </View>
        <View style={CardStyle.itemDetails}>
          <View style={CardStyle.top}>
            <Text style={CardStyle.topLeft}>{itemText}</Text>
            {
              isTrending &&
                <View style={CardStyle.topRight}/>
            }
          </View>
          <View style={CardStyle.middle}>
            <Text style={{fontSize: 16, fontFamily: "Iowan Old Style", color: "#000", fontWeight: '600'}}>{itemHeadline}</Text>
            <Text style={{marginTop: 10}}>{imgName}</Text>
          </View>
          <View style={CardStyle.bottom}>
            {
              expiryDate !== null &&
                <Text style={{fontSize: 12, color: '#474747'}}>Ending {moment().from(expiryDate)}</Text>
            }
          </View>
        </View>
      </View>
    </Card>
  );
}
