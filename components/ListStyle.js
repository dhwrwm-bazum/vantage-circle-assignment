import { StyleSheet, Platform, Dimensions } from 'react-native';

const extraTopMargin = Platform.OS === "ios" ? 20 : 0;


const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop:extraTopMargin,
      backgroundColor: '#FFF',
    }
  });

  export default styles;