import { StyleSheet, Platform, Dimensions } from 'react-native';

const extraTopMargin = Platform.OS === "ios" ? 20 : 0;


const styles = StyleSheet.create({
    container: {
      alignItems: 'center',
      backgroundColor: 'white',
      flexDirection: 'row',
      padding: 10
    },
    imgContainer: {
      flex: 0.4,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#33333D',
      borderRadius: 10
    },
    itemLogo: {
      width: 130,
      height: 130
    },
    itemDetails: {
      flex: 0.6,
      marginLeft: 20
    },
    top:{
      flex: 0.4,
      justifyContent: 'space-between',
      flexDirection: 'row',
      alignItems: 'flex-start',
      alignSelf: 'stretch'
    },
    topLeft:{
      flex: 0.9,
      fontFamily: "Iowan Old Style", 
      color: "#474747"
    },
    topRight:{
      width: 10,
      height: 10,
      borderRadius: 5,
      backgroundColor: 'red'
    },
    middle:{
      flex: 0.4
    },
    bottom:{
      flex: 0.2,
      justifyContent: 'flex-end',
      alignItems: 'flex-end'
    }
  });

  export default styles;